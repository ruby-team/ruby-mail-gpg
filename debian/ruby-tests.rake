require 'rake/testtask'
require 'pathname'
require 'tmpdir'

def setup_gpghome
  gpghome_upstream = File.join File.dirname(File.dirname(__FILE__)), 'test', 'gpghome'
  gpghome_tmp = Dir.mktmpdir("ruby-mail-gpg-#{Time.now.to_i}-#{rand(100)}")
  ENV['GNUPGHOME'] = gpghome_tmp
  ENV['GPG_AGENT_INFO'] = '' # disable gpg agent
  sh "cp -r #{gpghome_upstream}/* #{gpghome_tmp}/"
end

task :default => ["mail_gpg:tests:setup", :test, "mail_gpg:tests:cleanup"]

namespace :mail_gpg do
  namespace :tests do
    task :setup do
      setup_gpghome
    end
    task :cleanup do
      sh 'gpgconf --kill gpg-agent'
      ENV['GNUPGHOME'] = nil
    end
  end
end

Rake::TestTask.new(:test) do |test|
  test.libs << 'test'
  test.test_files = FileList['test/**/*_test.rb']
  test.verbose = true
end
